Hello World Webapp
==================

Basic webapp that may server as a template for other projects.  The maven project has the servlet dependency and maven plugins for tomcat and jetty.


Install/Running
===============

mvn clean install
mvn tomcat7:run or mvn jetty:run or mvn wildfly:run


http://localhost:8080/hello


For wildfly, don't know how to set context root
http://localhost:8080/helloworld/hello

