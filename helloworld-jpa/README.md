Java Persistence Examples
=========================

Tests create a table called "Resource" per the org.example.Resource class and Task per org.exmaple.Task.

Configuration of database connection is done in src/main/resources/META-INF/persistence.xml.

Update the pom.xml to load either Hibernate or EclipseLink.


PostgresSQL
===========
Both Hibernate and EclipseLink generate the same PostgreSQL table:

CREATE TABLE "Resource"
(
  "resourceId" bigint NOT NULL,
  uri character varying(255),
  "versionDate" timestamp without time zone,
  CONSTRAINT "Resource_pkey" PRIMARY KEY ("resourceId")
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Resource"
  OWNER TO postgres;