package org.example;

/**
 * Example class with optimistic locking support and mapping supplied by the resources/META-INF/orm.xml file.
 *
 */
public class Resource {

    public Resource() {
    }

    public Resource(long resourceId) {
        this.resourceId = resourceId;
    }

    public long getResourceId() {
        return resourceId;
    }

    public void setResourceId(long resourceId) {
        this.resourceId = resourceId;
    }

    public String getURI() {
        return uri;
    }

    public void setURI(String uri) {
        this.uri = uri;
    }

    private long resourceId;
    private String uri;

    // used for optimistic locking
    // must be timestamp to meet JPA spec.
    // must have a Version mapping or attribute.
    java.sql.Timestamp versionDate;
}
