package org.example;

import static org.junit.Assert.assertEquals;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Test;

/**
 * Unit test
 */
public class JPATest {

    public JPATest() throws Exception {
        // load example persisten unit from resources/META-INF/persistence.xml.
        entityManagerFactory = Persistence.createEntityManagerFactory("example");
    }

    /**
     * This has been run with both Hibernate and EclipseLink. Running with
     * EclipseLink required the versionDate to be java.sql.Timestamp and not
     * java.util.Date. Hibernate was more lenient.
     *
     */
    @Test
    public void testResource() {
        final long resourceId = 1;

        {
            final EntityManager em = entityManagerFactory.createEntityManager();

            em.getTransaction().begin();
            final Resource resource = new Resource(resourceId);
            em.persist(resource);
            em.getTransaction().commit();

            System.out.println("Version: " + resource.versionDate);
            em.close();
        }

        {
            final EntityManager em = entityManagerFactory.createEntityManager();

            em.getTransaction().begin();
            final Resource resource = em.find(Resource.class, resourceId);
            resource.setURI("test:uri4");
            em.getTransaction().commit();

            System.out.println("Version: " + resource.versionDate);
            em.close();
        }

    }

    /**
     * Example of non-transactional
     */
    @Test
    public void testTask() {
        {
            final EntityManager em = entityManagerFactory.createEntityManager();

            // adding should be done within a transaction
            em.getTransaction().begin();
            final Task task = new Task("A1", "Do Dishes");
            em.persist(task);
            em.getTransaction().commit();

            // could be other stuff here.

            em.close();
        }

        {
            final EntityManager em = entityManagerFactory.createEntityManager();

            // example of non-transactional read.
            final Task task = em.find(Task.class, "A1");
            assertEquals("A1", task.getId());

            // could be other stuff here.

            em.close();
        }

    }

    // locals, entityManagerFactory is thread safe
    private final EntityManagerFactory entityManagerFactory;
}
